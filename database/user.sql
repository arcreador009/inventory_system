-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2018 at 01:32 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_inv`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(300) NOT NULL,
  `usertype` enum('Admin','Other') NOT NULL,
  `register_date` date NOT NULL,
  `last_login` datetime NOT NULL,
  `notes` varchar(2555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `usertype`, `register_date`, `last_login`, `notes`) VALUES
(1, 'Avinash', 'avi@gmail.com', '$2y$08$D9R8.Og9km/d7fp2d9oKWuUvFV6He9.40ye8n.6QIxpTDiqZjoChu', 'Admin', '2018-12-14', '2018-12-14 00:00:00', ''),
(2, 'Ankush', 'avi1@gmail.com', '$2y$08$EXd0YA2R0zkX2y.ImnJnge/aPNlI8miACxz/uDVfVjJRPUZlMENya', 'Admin', '2018-12-14', '2018-12-14 00:00:00', ''),
(3, 'sujit', 'avi2@gmail.com', '$2y$08$rI1ZjUfOjEWk4y2cLgpvSuNpDdyRxBco3gXyoXiLR1KR1GK8plhkS', 'Admin', '2018-12-14', '2018-12-14 00:00:00', ''),
(4, 'prasad', 'avi3@gmail.com', '$2y$08$vyFBWPd2LfHyhUN7Mooy7OO.Z1UXy4Pm10nlPWCUokYR4YGxwSJeG', 'Admin', '2018-12-14', '2018-12-14 03:12:26', ''),
(5, 'test', 'avi4@gmail.com', '$2y$08$bDc0t3vrnDwAIwVsKLpuT.NYZrx2q8oUvpDEL7prNDzEodoHzcmYa', 'Admin', '2018-12-14', '2018-12-14 03:12:38', ''),
(6, 'test', 'avi5@gmail.com', '$2y$08$Z/xYNmu6vK445ncgITWkSuZTxyHhIdrEk5jo/rEBWS1xPXXmJKKlC', 'Admin', '2018-12-14', '2018-12-14 03:12:56', ''),
(8, 'avinash', 'ar1@gmail.com', '$2y$08$c2FHQvNpCIMx6A11BUt3L.rCoqM/zXT2TZjcfjvCthYcaZABXAPhe', 'Admin', '2018-12-15', '2018-12-15 00:00:00', ''),
(9, 'avinash', 'arp1@gmail.com', '$2y$08$V5hHItU6miJEXUIxMizKvejmoMl1PLiE6hQCpAm2WRrrbdpImrm76', 'Admin', '2018-12-15', '2018-12-15 00:00:00', ''),
(10, 'avinash', 'arpp1@gmail.com', '$2y$08$IO0z4Hj9zP6gxwKPDWxRYuvQLVp2xtnYo6V02q9YpAV4gotdNlkSG', 'Admin', '2018-12-15', '2018-12-15 09:12:24', ''),
(11, 'avi', 'pp1@gmail.com', '$2y$08$pjWYzFSsXKgNe2XkkC03aeOE2vbdnhijhNMIbJF9gtyUJsLiiiD.C', 'Admin', '2018-12-15', '2018-12-15 09:12:44', ''),
(12, 'avi', 'ppp1@gmail.com', '$2y$08$e/KQA4OdZDQqd2TJw05U5.HvSfrnJ7SWV672zH1/weF3Aww8jLSBK', 'Admin', '2018-12-15', '2018-12-15 12:12:54', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
