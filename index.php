<?php
include_once("./database/constant.php");

if (isset($_SESSION["userid"])) {
	header("location:".DOMAIN."/dashboard.php");
}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>inventory system</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="./include/style.css">

	<script type="text/javascript" link="stylesheet" src="./js/main.js"></script>
</head>
<body>
	<div class="overlay"><div class="loader"></div></div>
	<!--Navbar here-->
	<?php include_once("./template/header.php"); ?>
<br/><br/>
<div class="container">
	<?php
		if (isset($_GET["msg"]) AND !empty($_GET["msg"])) {
			?>
			<div class="alert alert-success alert-dismissible fade show" role="alert">
	 			 <?php echo $_GET["msg"];?>
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    			<span aria-hidden="true">&times;</span>
	 		 	</button>
			</div>
<?php
		}
	?>
	<div class="card mx-auto" style="width: 18rem;">
		 <img class="card-img-top mx-auto" style="width:60%;" src="./image/login.png" alt="Login Icon">
		<div class="card-body">
			<form id="form_login" onsubmit="return false">
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" name="log_email" id="log_email" placeholder="Enter email">
					<small id="e_error" class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<input type="password" class="form-control" name="log_password" id="log_password" placeholder="Password">
					<small id="p_error" class="form-text text-muted"></small>
				</div>
				<button type="submit" class="btn btn-primary"><i class="fa fa-lock">&nbsp;</i>Login</button>
				<span><a href="register.php">Register</a></span>
			</form>
		</div>
		<div class="card-footer"><a href="#">Forgrt Password ?</a></div>
	</div>
</div>


</body>
</html>
